# Mastering Unity 2D Game Development (Second Edition) - Reader Files

This repository contains the files for readers of the book [Mastering Unity 2D Game Development (Second Edition)](https://www.packtpub.com/game-development/mastering-unity-2d-game-development-second-edition). 
I created this repository so that I could easily update the files based on reader feedback.
